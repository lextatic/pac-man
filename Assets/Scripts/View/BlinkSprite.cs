﻿using UnityEngine;

public class BlinkSprite : MonoBehaviour
{
	private float nextStateChange;
	
	public SpriteRenderer spriteRenderer;

	public float Interval;

    // Start is called before the first frame update
    void Start()
    {
		spriteRenderer.enabled = true;
		nextStateChange = Time.time + Interval;
	}

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextStateChange)
		{
			SwitchState();
		}
    }

	void SwitchState()
	{
		spriteRenderer.enabled = !spriteRenderer.enabled;
		nextStateChange = Time.time + Interval;
	}
}
