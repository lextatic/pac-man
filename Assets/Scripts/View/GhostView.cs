﻿using UnityEngine;

public class GhostView : MonoBehaviour
{
	private Direction previousDirection;

	private GhostState previousGhostState;

	public Ghost GhostBehaviour;

	public Animator GhostAnimator;

	private void Start()
	{
		GhostAnimator.SetInteger("GhostType", (int)GhostBehaviour.GhostType);
		GhostAnimator.SetInteger("State", (int)GhostBehaviour.GhostState);
	}

	private void Update()
	{
		if (previousDirection != GhostBehaviour.CharacterMotor.CurrentDirection)
		{
			GhostAnimator.SetInteger("Direction", (int)GhostBehaviour.CharacterMotor.CurrentDirection - 1);
		}

		if(previousGhostState != GhostBehaviour.GhostState)
		{
			GhostAnimator.SetInteger("State", (int)GhostBehaviour.GhostState);
		}

		previousDirection = GhostBehaviour.CharacterMotor.CurrentDirection;
		previousGhostState = GhostBehaviour.GhostState;
	}
}
