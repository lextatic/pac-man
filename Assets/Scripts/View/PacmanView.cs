﻿using UnityEngine;
using UnityEngine.Playables;

public class PacmanView : MonoBehaviour
{
	private Vector3 previousPosition;

	public CharacterMotor CharacterMotor;

	public SpriteRenderer SpriteRenderer;

	public PlayableDirector MoveAnimationDirector;

	private bool playingDeathAnimation;

	private float startDeathAnimationTime;

	private void Start()
	{


		previousPosition = transform.position;
		playingDeathAnimation = false;

		MoveAnimationDirector.time = 0f;
		MoveAnimationDirector.Evaluate();
	}

	private void Update()
    {
		if (playingDeathAnimation)
		{
			MoveAnimationDirector.time = 0.15625f + Time.time - startDeathAnimationTime;
			MoveAnimationDirector.Evaluate();
		}
		else
		{
			switch (CharacterMotor.CurrentDirection)
			{
				default:
				case Direction.NONE:
					break;

				case Direction.RIGHT:
					transform.right = Vector3.right;
					break;

				case Direction.LEFT:
					transform.right = Vector3.left;
					break;

				case Direction.UP:
					transform.right = Vector3.up;
					break;

				case Direction.DOWN:
					transform.right = Vector3.down;
					break;
			}

			if (previousPosition != transform.position)
			{
				// Not perfect but will do it (animation is so fast you won't even notice it's not continuing from where it stopped)
				MoveAnimationDirector.time = Time.time % 0.15625f;
				MoveAnimationDirector.Evaluate();
			}

			previousPosition = transform.position;
		}
	}

	public void PlayDeathAnimation()
	{
		transform.right = Vector3.right;
		playingDeathAnimation = true;
		startDeathAnimationTime = Time.time;
	}

	public void EndDeathAnimation()
	{
		playingDeathAnimation = false;
		MoveAnimationDirector.time = 0f;
		MoveAnimationDirector.Evaluate();
		previousPosition = transform.position;
	}

	public void Hide()
	{
		SpriteRenderer.enabled = false;
	}

	public void Show()
	{
		SpriteRenderer.enabled = true;
	}
}
