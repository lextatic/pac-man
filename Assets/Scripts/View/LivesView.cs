﻿using UnityEngine;

public class LivesView : MonoBehaviour
{
	public GameObject[] LivesSprites;

	public GameManager GameManager;

	private void Start()
	{
		GameManager.OnLifeAdded += AddLife;
		GameManager.OnLifeReduced += ReduceLife;

		for(int i = 0; i < LivesSprites.Length; i++)
		{
			if(i < GameManager.Lives.Value)
			{
				LivesSprites[i].SetActive(true);
			}
			else
			{
				LivesSprites[i].SetActive(false);
			}
		}
	}

	private void AddLife()
    {
		LivesSprites[GameManager.Lives.Value + 1].SetActive(true);
	}

	private void ReduceLife()
	{
		LivesSprites[GameManager.Lives.Value].SetActive(false);
	}
}
