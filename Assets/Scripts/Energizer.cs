﻿using UnityEngine;

public class Energizer : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		FindObjectOfType<GameManager>().ActivateEnergizer();
	}
}
