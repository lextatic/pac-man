﻿using TMPro;
using UnityEngine;

public class InstantiateScoreOnCollect : MonoBehaviour
{
	public IntValue Score;

	public TextMeshPro ScoreText;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var scoreText = Instantiate(ScoreText, transform.position, Quaternion.identity);
		scoreText.text = Score.Value.ToString();
	}
}
