﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class SwitchMap : MonoBehaviour
{
	public Tilemap GameTileMap;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var ghost = collision.GetComponent<Ghost>();
		if (ghost.GhostState != GhostState.Defeated && ghost.CharacterMotor.Map != GameTileMap)
		{
			ghost.CharacterMotor.Map = GameTileMap;
			ghost.ApplyCurrentGhostMethod();
		}
	}
}
