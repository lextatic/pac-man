﻿using System;
using UnityEngine;

public class Collectable : MonoBehaviour
{
	public IntValue Score;
	public IntValue PlayerScore;
	public int FramesToSkip;

	public Action Collected;

	public bool IsVictoryCondition = false;

	public AudioSource audioSource;

	public SimpleAudioEvent audioEvent;

	private void Start()
	{
		if (IsVictoryCondition)
		{
			FindObjectOfType<GameManager>().RegisterForVictoryCondition(this);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (FramesToSkip > 0)
		{
			collision.GetComponent<CharacterMotor>().SetFrameSkips(FramesToSkip);
		}

		PlayerScore.Value += Score.Value;
		Collected?.Invoke();
		audioEvent.Play(audioSource);
		Destroy(gameObject);
	}
}
