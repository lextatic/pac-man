﻿using UnityEngine;

public class LockGhostUpDirection : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		collision.GetComponent<Ghost>().LockUpDirection = true;
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		collision.GetComponent<Ghost>().LockUpDirection = false;
	}
}
