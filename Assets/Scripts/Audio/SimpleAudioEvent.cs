﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Audio Events/Simple")]
public class SimpleAudioEvent : AudioEvent
{
	private int audioIndex;

	public AudioClip[] Clips;

	public float Volume = 1;

	public float Pitch = 1;

	public void Awake()
	{
		audioIndex = 0;
	}

	public override void Play(AudioSource source)
	{
		if (Clips.Length == 0) return;

		source.volume = Volume;
		source.pitch = Pitch;
		source.PlayOneShot(Clips[audioIndex]);
		audioIndex++;

		if (audioIndex == Clips.Length)
		{
			audioIndex = 0;
		}
	}
}