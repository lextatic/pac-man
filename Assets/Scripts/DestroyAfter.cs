﻿using System.Collections;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
	public float Time;

    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(DelayedDestroy());
    }

    // Update is called once per frame
    private IEnumerator DelayedDestroy()
    {
		yield return new WaitForSeconds(Time);

		Destroy(gameObject);
    }
}
