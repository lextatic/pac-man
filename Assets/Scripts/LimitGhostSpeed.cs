﻿using UnityEngine;

public class LimitGhostSpeed : MonoBehaviour
{
	public float SpeedLimit;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var ghost = collision.GetComponent<Ghost>();
		if (ghost.GhostState != GhostState.Defeated)
		{
			ghost.CharacterMotor.CurrentSpeedPercentage = SpeedLimit;
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		var ghost = collision.GetComponent<Ghost>();
		
		switch(ghost.GhostState)
		{
			default:
			case GhostState.Active:
				ghost.CharacterMotor.CurrentSpeedPercentage = 0.75f;
				break;

			case GhostState.Vulnerable:
			case GhostState.VulnerabilityEnding:
				ghost.CharacterMotor.CurrentSpeedPercentage = 0.5f;
				break;

			case GhostState.Defeated:
				break;
		}
	}
}
