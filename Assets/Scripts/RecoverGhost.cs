﻿using UnityEngine;

public class RecoverGhost : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		collision.GetComponent<Ghost>().Recover();
	}
}
