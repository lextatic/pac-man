﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Int Value")]
public class IntValue : ScriptableObject
{
	public int Value;
}
