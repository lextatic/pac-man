﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/GhostTargeting/Direct Chase")]
public class DirectChase : GetTargetMethod
{
	private CharacterMotor pacmanMotor;

	private Transform pacmanTransform;

	public float DistanceToLookAhead;

	public override void Initialize()
	{
		pacmanTransform = GameObject.FindGameObjectWithTag("Player").transform;
		pacmanMotor = pacmanTransform.GetComponent<CharacterMotor>();
	}

	public override Vector3 GetTarget()
	{
		var offset = Vector3.zero;

		switch(pacmanMotor.CurrentDirection)
		{
			default:
			case Direction.NONE:
			case Direction.UP:
				offset.y = DistanceToLookAhead;
				offset.x = -DistanceToLookAhead; // Yes, original Pacman has this bug and I'm implementing it here too
				break;

			case Direction.LEFT:
				offset.x = -DistanceToLookAhead;
				break;

			case Direction.DOWN:
				offset.y = -DistanceToLookAhead;
				break;

			case Direction.RIGHT:
				offset.x = DistanceToLookAhead;
				break;
		}

		return pacmanTransform.position + offset;
	}
}
