﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/GhostTargeting/Shy Ghost")]
public class ShyGhost : GetTargetMethod
{
	private CharacterMotor pacmanMotor;

	private Transform pacmanTransform;

	private Transform myTransform;

	public Vector3Int ScatterTarget;

	public float DistanceToLookAhead;

	public float BailDistance;

	public override void Initialize()
	{
		pacmanTransform = GameObject.FindGameObjectWithTag("Player").transform;
		pacmanMotor = pacmanTransform.GetComponent<CharacterMotor>();
	}

	public override Vector3 GetTarget()
	{
		var offset = Vector3.zero;

		switch(pacmanMotor.CurrentDirection)
		{
			default:
			case Direction.NONE:
			case Direction.UP:
				offset.y = DistanceToLookAhead;
				offset.x = -DistanceToLookAhead; // Yes, original Pacman has this bug and I'm implementing it here too
				break;

			case Direction.LEFT:
				offset.x = -DistanceToLookAhead;
				break;

			case Direction.DOWN:
				offset.y = -DistanceToLookAhead;
				break;

			case Direction.RIGHT:
				offset.x = DistanceToLookAhead;
				break;
		}

		if(Vector3.Distance(GhostBehaviour.transform.position, pacmanTransform.position) < BailDistance)
		{
			return GhostBehaviour.CharacterMotor.Map.CellToWorld(ScatterTarget);
		}

		return pacmanTransform.position + offset;
	}
}
