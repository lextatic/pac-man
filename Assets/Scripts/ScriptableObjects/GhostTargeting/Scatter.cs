﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/GhostTargeting/Scatter")]
public class Scatter : GetTargetMethod
{
	public Vector3Int Target;

	public override void Initialize() { }

	public override Vector3 GetTarget()
	{
		return GhostBehaviour.CharacterMotor.Map.CellToWorld(Target);
	}
}
