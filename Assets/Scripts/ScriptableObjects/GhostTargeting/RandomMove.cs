﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/GhostTargeting/Random Move")]
public class RandomMove : GetTargetMethod
{
	public override void Initialize() {	}

	public override Vector3 GetTarget()
	{
		Vector3 offset = Vector3.zero;
		switch (Random.Range(0, 4))
		{
			default:
			case 0:
				offset.x = 1;
				break;

			case 1:
				offset.x = -1;
				break;

			case 2:
				offset.y = 1;
				break;

			case 3:
				offset.y = -1;
				break;
		}

		return GhostBehaviour.transform.position + offset;
	}
}
