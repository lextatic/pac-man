﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/GhostTargeting/Mirror Position")]
public class MirrorPosition : GetTargetMethod
{
	private Transform pacmanTransform;

	private Transform mirroGhostTransform;

	private CharacterMotor pacmanMotor;

	public float DistanceToLookAhead;

	public GhostType GhostToMirror;

	public override void Initialize()
	{
		pacmanTransform = GameObject.FindGameObjectWithTag("Player").transform;
		mirroGhostTransform = GameObject.FindGameObjectWithTag(GhostToMirror.ToString()).transform;
		pacmanMotor = pacmanTransform.GetComponent<CharacterMotor>();
	}

	public override Vector3 GetTarget()
	{
		var offset = Vector3.zero;

		switch(pacmanMotor.CurrentDirection)
		{
			default:
			case Direction.NONE:
			case Direction.UP:
				offset.y = DistanceToLookAhead;
				offset.x = -DistanceToLookAhead; // Yes, original Pacman has this bug and I'm implementing it here too
				break;

			case Direction.LEFT:
				offset.x = -DistanceToLookAhead;
				break;

			case Direction.DOWN:
				offset.y = -DistanceToLookAhead;
				break;

			case Direction.RIGHT:
				offset.x = DistanceToLookAhead;
				break;
		}

		var mirrorPoint = pacmanTransform.position + offset;

		return mirrorPoint + mirrorPoint - mirroGhostTransform.position;
	}
}
