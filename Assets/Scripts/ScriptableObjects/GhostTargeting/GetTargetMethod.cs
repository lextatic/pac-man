﻿using UnityEngine;

public abstract class GetTargetMethod : ScriptableObject
{
	[HideInInspector]
	public Ghost GhostBehaviour;

	public abstract void Initialize();
	public abstract Vector3 GetTarget();
}
