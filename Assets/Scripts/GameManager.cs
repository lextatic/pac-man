﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
	private enum GameState
	{
		READY,
		PLAYING,
		GAME_OVER
	}

	private static bool NewGame = false;

	private int scatterChaseTimerIndex;

	private int victoryConditionCount;

	private int releaseGhostIndex;

	private Vector3 pacmanStartPosition;

	private Coroutine ScatterChaseCoroutine;

	private int energyzerMultiplier;

	private GameState gameState;

	public IntValue PlayerScore;

	public Ghost[] Ghosts;

	public Pacman Pacman;

	public float[] ScatterChaseTimers;

	public int[] ReleaseGhostsDotCount;

	public int Fruit1DotCount;

	public int Fruit2DotCount;

	public Collectable Fruit1;

	public Collectable Fruit2;

	public Transform FruitSpawnPosition;

	public float EnergizerDuration = 10f;

	public int BaseGhostScore = 200;

	public IntValue Lives;

	public int MaxLives = 5;

	public Action OnLifeReduced;

	public Action OnLifeAdded;

	public TextMeshPro ScoreText;

	public GameObject ReadyText;

	public GameObject GameOverText;

	public Tilemap LevelTilemap;

	public AudioSource AudioSource;

	public SimpleAudioEvent BeginningAudioEvent;

	private void OnApplicationQuit()
	{
		PlayerScore.Value = 0;
		Lives.Value = 3;
	}

	private void Awake()
	{
		if (NewGame)
		{
			PlayerScore.Value = 0;
			Lives.Value = 3;
		}
	}

	private void Start()
    {
		ScatterChaseCoroutine = StartCoroutine(Chase(ScatterChaseTimers[scatterChaseTimerIndex]));
		pacmanStartPosition = Pacman.transform.position;
		Pacman.OnDeath += PacmanCaught;
		for(int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].OnDefeat += GhostDefeated;
		}

		GameOverText.SetActive(false);

		BeginningAudioEvent.Play(AudioSource);

		releaseGhostIndex = 0;

		StartCoroutine(StartGame(4.3f));
	}

	private IEnumerator StartGame(float delay)
	{
		ReadyText.SetActive(true);
		
		Pacman.CharacterMotor.enabled = false;
		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].CharacterMotor.enabled = false;
		}
		gameState = GameState.READY;

		yield return new WaitForSeconds(delay);

		ReadyText.SetActive(false);
		gameState = GameState.PLAYING;
		Pacman.CharacterMotor.enabled = true;
		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].CharacterMotor.enabled = true;
		}
	}

	private void Update()
	{
		if(gameState == GameState.GAME_OVER && Input.anyKeyDown)
		{
			NewGame = true;
			SceneManager.LoadScene(0);
		}
	}

	private IEnumerator Chase(float delay)
	{
		scatterChaseTimerIndex++;

		yield return new WaitForSeconds(delay);

		for(int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].Chase();
		}

		if (scatterChaseTimerIndex < ScatterChaseTimers.Length)
		{
			ScatterChaseCoroutine = StartCoroutine(Scatter(ScatterChaseTimers[scatterChaseTimerIndex]));
		}
	}

	private IEnumerator Scatter(float delay)
	{
		scatterChaseTimerIndex++;

		yield return new WaitForSeconds(delay);

		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].Scatter();
		}

		if (scatterChaseTimerIndex < ScatterChaseTimers.Length)
		{
			ScatterChaseCoroutine = StartCoroutine(Chase(ScatterChaseTimers[scatterChaseTimerIndex]));
		}
	}

	public void RegisterForVictoryCondition(Collectable collectible)
	{
		collectible.Collected += VictoryConditionCollectableCollected;
		victoryConditionCount++;
	}

	private void VictoryConditionCollectableCollected()
	{
		victoryConditionCount--;

		if(releaseGhostIndex < ReleaseGhostsDotCount.Length && victoryConditionCount == ReleaseGhostsDotCount[releaseGhostIndex])
		{
			Ghosts[releaseGhostIndex + 1].Release();
			releaseGhostIndex++;
		}

		if(victoryConditionCount == Fruit1DotCount)
		{
			var fruit = Instantiate(Fruit1, FruitSpawnPosition.position, Quaternion.identity);
			fruit.audioSource = AudioSource;
		}

		if (victoryConditionCount == Fruit2DotCount)
		{
			var fruit = Instantiate(Fruit2, FruitSpawnPosition.position, Quaternion.identity);
			fruit.audioSource = AudioSource;
		}

		if (victoryConditionCount == 0)
		{
			StartCoroutine(Victory());
		}
	}

	public void ActivateEnergizer()
	{
		Pacman.Energyze(EnergizerDuration);

		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].RunAway(EnergizerDuration, true);
		}

		energyzerMultiplier = 1;
	}

	public IEnumerator Victory()
	{
		// TODO: increment difficulty

		for (int i = 0; i < Ghosts.Length; i++)
		{
			var ghost = Ghosts[i];
			ghost.HideGhost();
			ghost.CharacterMotor.enabled = false;
		}

		Pacman.CharacterMotor.enabled = false;

		var waitForSeconds = new WaitForSeconds(0.2f);

		yield return waitForSeconds;
		LevelTilemap.color = Color.white;
		yield return waitForSeconds;
		LevelTilemap.color = Color.blue;
		yield return waitForSeconds;
		LevelTilemap.color = Color.white;
		yield return waitForSeconds;
		LevelTilemap.color = Color.blue;
		yield return waitForSeconds;
		LevelTilemap.color = Color.white;
		yield return waitForSeconds;
		LevelTilemap.color = Color.blue;
		yield return waitForSeconds;


		NewGame = false;
		SceneManager.LoadScene(0);
	}

	private void PacmanCaught()
	{
		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].Deactivate();
		}

		if (ScatterChaseCoroutine != null)
		{
			StopCoroutine(ScatterChaseCoroutine);
		}

		if (Lives.Value > 1)
		{
			StartCoroutine(RestartCoroutine());
		}
		else
		{
			StartCoroutine(Defeat());
		}
	}

	public void DecreaseLife()
	{
		if (Lives.Value != 1)
		{
			Lives.Value--;
			OnLifeReduced?.Invoke();
		}
	}

	public void AddLife()
	{
		if(Lives.Value != MaxLives)
		{
			Lives.Value++;
			OnLifeAdded?.Invoke();
		}
	}

	public void GhostDefeated(Ghost ghost)
	{
		PlayerScore.Value += BaseGhostScore * energyzerMultiplier;

		energyzerMultiplier *= 2;

		StartCoroutine(ShowScoreCoroutine(ghost));
	}

	private IEnumerator ShowScoreCoroutine(Ghost ghost)
	{
		ghost.HideGhost();
		for(int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].CharacterMotor.enabled = false;
		}

		Pacman.HidePacman();
		Pacman.CharacterMotor.enabled = false;

		var scoreText = Instantiate(ScoreText, ghost.transform.position, Quaternion.identity);
		scoreText.text = $"{(BaseGhostScore * energyzerMultiplier / 2):0}";

		yield return new WaitForSeconds(1f);

		Destroy(scoreText.gameObject);

		ghost.ShowGhost();
		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].CharacterMotor.enabled = true;
		}

		Pacman.ShowPacman();
		Pacman.CharacterMotor.enabled = true;
	}

	private IEnumerator RestartCoroutine()
	{
		yield return new WaitForSeconds(3f);
		Pacman.ResetPacman();
		for (int i = 0; i < Ghosts.Length; i++)
		{
			Ghosts[i].ResetGhost();
		}
		DecreaseLife();
		StartCoroutine(StartGame(2f));
	}

	public IEnumerator Defeat()
	{
		yield return new WaitForSeconds(3f);

		DecreaseLife();

		GameOverText.SetActive(true);
		gameState = GameState.GAME_OVER;
	}
}
