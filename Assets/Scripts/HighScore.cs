﻿using TMPro;
using UnityEngine;

public class HighScore : MonoBehaviour
{
	private int highScore;

	public IntValue PlayerScore;

	public TextMeshProUGUI HighScoreText;

	private void Start()
	{
		highScore = PlayerPrefs.GetInt("HighScore", 0);
	}

	private void Update()
	{
		if(PlayerScore.Value > highScore)
		{
			highScore = PlayerScore.Value;
		}

		HighScoreText.text = $"{highScore:00}";
	}

	private void OnDestroy()
	{
		PlayerPrefs.SetInt("HighScore", highScore);
	}
}
