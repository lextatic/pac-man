﻿using TMPro;
using UnityEngine;

public class GameScore : MonoBehaviour
{
	public IntValue PlayerScore;

	public TextMeshProUGUI Text;

	private void Update()
	{
		// This is actually a view behaviour, but okay here
		Text.text = $"{PlayerScore.Value:00}";
	}
}
