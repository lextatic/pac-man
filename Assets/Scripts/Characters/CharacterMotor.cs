﻿using UnityEngine;
using UnityEngine.Tilemaps;

public enum Direction
{
	NONE,
	UP,
	LEFT,
	DOWN,
	RIGHT
}

/// <summary>
/// Moves the character towards the next tile with desired speed
/// </summary>
public class CharacterMotor : MonoBehaviour
{
	private int frameSkips;

	private float movementCompensation;

	[HideInInspector]
	public readonly Vector3 CellOffset = new Vector3(0.5f, 0.5f, 0);

	[HideInInspector]
	public Vector3Int NextTile;

	//[HideInInspector]
	public Direction CurrentDirection;

	public Tilemap Map;

	public float MaxSpeed;

	[HideInInspector]
	public float CurrentSpeedPercentage;

	private float CurrentSpeed
	{
		get
		{
			return MaxSpeed * CurrentSpeedPercentage;
		}
	}


	void Start()
    {
		NextTile = Map.WorldToCell(transform.position + Vector3.left);
		frameSkips = 0;
		movementCompensation = 0;
	}
		
	private void FixedUpdate()
	{
		// Had to include this to make player keep its initial position, could probably think of a smarter way though
		if (CurrentDirection == Direction.NONE) return;

		// This is used to make Pacman slows down a little when eating dots
		if(frameSkips > 0)
		{
			frameSkips--;
			return;
		}

		Vector3 newPosition = transform.position;
		var diff = (Map.CellToWorld(NextTile) + CellOffset).x - transform.position.x;
		var absDiff = Mathf.Abs(diff);
		if (absDiff > CurrentSpeed * Time.fixedDeltaTime)
		{
			newPosition.x = (newPosition.x + CurrentSpeed * Time.fixedDeltaTime * Mathf.Sign(diff)) + movementCompensation;
			movementCompensation = 0;
		}
		else if (diff != 0)
		{
			// Move to align perfectly with the target tile
			newPosition.x = newPosition.x + diff;
			// This causes a loss in velocity, to counter that we save a movementCompensation that we apply on the next frame (should probably be interpolating positions on Update)
			movementCompensation = (CurrentSpeed * Time.fixedDeltaTime - absDiff) * Mathf.Sign(diff);
		}

		diff = (Map.CellToWorld(NextTile) + CellOffset).y - transform.position.y;
		absDiff = Mathf.Abs(diff);
		if (absDiff > CurrentSpeed * Time.fixedDeltaTime)
		{
			newPosition.y = (newPosition.y + CurrentSpeed * Time.fixedDeltaTime * Mathf.Sign(diff)) + movementCompensation;
			movementCompensation = 0;
		}
		else if (diff != 0)
		{
			newPosition.y = newPosition.y + diff;
			movementCompensation = (CurrentSpeed * Time.fixedDeltaTime - absDiff) * Mathf.Sign(diff);
		}

		transform.position = newPosition;
	}

	/// <summary>
	/// Gets an offset based on this motor's CurrentDirection
	/// </summary>
	/// <returns>One tile offset</returns>
	public Vector3 GetOffset()
	{
		return GetOffset(CurrentDirection);
	}

	/// <summary>
	/// Gets a one tile offset based on a direction
	/// </summary>
	/// <param name="direction">Direction to consider</param>
	/// <returns>One tile offset</returns>
	public Vector3 GetOffset(Direction direction)
	{
		Vector3 offset = Vector3.zero;
		switch (direction)
		{
			default:
			case Direction.NONE:
				break;

			case Direction.RIGHT:
				offset.x = 1;
				break;

			case Direction.LEFT:
				offset.x = -1;
				break;

			case Direction.UP:
				offset.y = 1;
				break;

			case Direction.DOWN:
				offset.y = -1;
				break;
		}

		return offset;
	}

	/// <summary>
	/// Checks if the targetPosition is a valid position
	/// </summary>
	/// <param name="position"></param>
	/// <returns>true if valid; false otherwise</returns>
	public bool IsValidPosition(Vector3 position)
	{
		return Map.HasTile(Map.WorldToCell(position));
	}

	public void SetFrameSkips(int framesToSkip)
	{
		frameSkips = framesToSkip;
	}
}
