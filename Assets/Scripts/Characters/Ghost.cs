﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum GhostType
{
	Blinky,
	Pinky,
	Inky,
	Clyde
}

public enum GhostState
{
	Active,
	Vulnerable,
	VulnerabilityEnding,
	Defeated
}

[RequireComponent(typeof(CharacterMotor))]
public class Ghost : MonoBehaviour
{
	private Vector3 _currentTileWorldPosition;
	private Vector3Int _currentTile;
	private List<Vector3> _validPositions;
	private List<Vector3> _offsets;
	private Vector3 targetPosition;
	private Vector3 initialPosition;
	private GetTargetMethod currentGetTargetMethod;
	private float vulnerabilityEndTime;
	private bool isChasing;
	private bool frightened;
	private bool freeMove;
	private bool inGhostHouse;

	public bool isVulnerable
	{
		get
		{
			return GhostState == GhostState.Vulnerable || GhostState == GhostState.VulnerabilityEnding;
		}
	}

	public CharacterMotor CharacterMotor { get; private set; }

	public GhostType GhostType;

	[HideInInspector]
	public GhostState GhostState;

	public GetTargetMethod ScatterMethod;
	public GetTargetMethod ChaseMethod;
	public GetTargetMethod FrightenedMethod;
	public GetTargetMethod DefeatedMethod;
	public GetTargetMethod LeaveGhostHouseMethod;
	public GetTargetMethod StartupMethod;

	public Tilemap GhostsOnlyTilemap;

	[HideInInspector]
	public bool LockUpDirection;

	public float FreeEndingWarningTime = 2f;

	public Action<Ghost> OnDefeat;

	public AudioSource audioSource;

	public SimpleAudioEvent ghostCaughtEvent;

	private void Awake()
	{
		CharacterMotor = GetComponent<CharacterMotor>();
	}

	private void Start()
	{	
		CharacterMotor.CurrentSpeedPercentage = 0.75f;

		GhostState = GhostState.Active;

		_validPositions = new List<Vector3>();
		_offsets = new List<Vector3>();

		initialPosition = transform.position;

		UpdateCurrentTile(CharacterMotor.Map.WorldToCell(transform.position));
		
		ChangeDirection(Direction.LEFT, false);
		ChooseDirection();

		ScatterMethod = Instantiate(ScatterMethod);
		ScatterMethod.GhostBehaviour = this;
		ScatterMethod.Initialize();

		ChaseMethod = Instantiate(ChaseMethod);
		ChaseMethod.GhostBehaviour = this;
		ChaseMethod.Initialize();

		FrightenedMethod = Instantiate(FrightenedMethod);
		FrightenedMethod.GhostBehaviour = this;
		FrightenedMethod.Initialize();

		DefeatedMethod = Instantiate(DefeatedMethod);
		DefeatedMethod.GhostBehaviour = this;
		DefeatedMethod.Initialize();

		LeaveGhostHouseMethod = Instantiate(LeaveGhostHouseMethod);
		LeaveGhostHouseMethod.GhostBehaviour = this;
		LeaveGhostHouseMethod.Initialize();

		StartupMethod = Instantiate(StartupMethod);
		StartupMethod.GhostBehaviour = this;
		StartupMethod.Initialize();

		currentGetTargetMethod = StartupMethod;
		isChasing = false;
		frightened = false;
		freeMove = false;
		inGhostHouse = true;

		LockUpDirection = false;
	}

	private void Update()
	{
		if(GhostState == GhostState.Vulnerable && Time.time > vulnerabilityEndTime - FreeEndingWarningTime)
		{
			GhostState = GhostState.VulnerabilityEnding;
		}

		if (frightened && Time.time > vulnerabilityEndTime)
		{
			GhostState = GhostState.Active; // questionable (is it defeated?)
			if (isChasing)
			{
				currentGetTargetMethod = ChaseMethod;
			}
			else
			{
				currentGetTargetMethod = ScatterMethod;
			}

			CharacterMotor.CurrentSpeedPercentage = 0.75f;
			frightened = false;
		}
	}

	void FixedUpdate()
	{
		var newTilePosition = CharacterMotor.Map.WorldToCell(transform.position);

		// Ghosts make their decisions as soon as they enter a new tile
		if(newTilePosition != _currentTile)
		{
			targetPosition = currentGetTargetMethod.GetTarget();
			UpdateCurrentTile(newTilePosition);
			ChooseDirection();
		}

		UpdateTargetTile();
	}

	public void Release()
	{
		CharacterMotor.Map = GhostsOnlyTilemap;
		currentGetTargetMethod = LeaveGhostHouseMethod;
	}

	/// <summary>
	/// As _currentTileWorldPosition is necessary various times during this algorithm we cache this value everytime we change our current tile so we don't have to recalculate it everytime
	/// </summary>
	private void UpdateCurrentTile(Vector3Int newTile)
	{
		_currentTile = newTile;
		_currentTileWorldPosition = CharacterMotor.Map.CellToWorld(_currentTile) + CharacterMotor.CellOffset;
	}

	/// <summary>
	/// Choses a new direction based on possible valid directions and target position. When distance from target is tied ghost prefers to move on the following order (up, left, down, right)
	/// </summary>
	private void ChooseDirection()
	{
		GetValidTiles();

		if (_validPositions.Count == 0)
		{
			UpdateCurrentTile(CharacterMotor.Map.WorldToCell(transform.position));
			ChangeDirection(CharacterMotor.CurrentDirection, true);
			GetValidTiles();
		}

		// At this point the _validPositions is ordered as we want, we only change targetPosition if it's closer to the target
		var tilePosition = _validPositions[0];
		for(int i = 1; i < _validPositions.Count; i++)
		{
			if(Vector3.SqrMagnitude(targetPosition - _validPositions[i]) < Vector3.SqrMagnitude(targetPosition - tilePosition))
			{
				tilePosition = _validPositions[i];
			}
		}

		//var tilePosition = _validPositions[0];
		//var targetTileDistance = Math.Abs(targetPosition.x - tilePosition.x) + Math.Abs(targetPosition.y - tilePosition.y);
		//for (int i = 1; i < _validPositions.Count; i++)
		//{
		//	var currentTileDistance = Math.Abs(targetPosition.x - _validPositions[i].x) + Math.Abs(targetPosition.y - _validPositions[i].y);
		//	Debug.Log($"currentTileDistance: {currentTileDistance}, targetTileDistance: {targetTileDistance}");
		//	if (currentTileDistance < targetTileDistance)
		//	{
		//		targetTileDistance = currentTileDistance;
		//		tilePosition = _validPositions[i];
		//	}
		//}

		// Set direction based on difference from current and target position
		// As ghosts are always aligned with grid we only need to check each individual direction
		var result = tilePosition - _currentTileWorldPosition;

		if (result.x == 1)
		{
			ChangeDirection(Direction.RIGHT);
		}
		else if (result.x == -1)
		{
			ChangeDirection(Direction.LEFT);
		}
		else if (result.y == 1 && (!LockUpDirection || freeMove))
		{
			ChangeDirection(Direction.UP);
		}
		else if (result.y == -1)
		{
			ChangeDirection(Direction.DOWN);
		}
	}

	/// <summary>
	/// Changes the direciton and updates all offsets. Ghosts don't turn back and have a preference for turning (up, left, down, right) and that's why we need to specify offsets this way.
	/// </summary>
	/// <param name="newDirection"></param>
	private void ChangeDirection(Direction newDirection, bool allowMoveBack = false)
	{
		if(allowMoveBack || inGhostHouse)
		{
			CharacterMotor.CurrentDirection = newDirection;
			_offsets.Clear();

			_offsets.Add(Vector3.up);
			_offsets.Add(Vector3.left);
			_offsets.Add(Vector3.down);
			_offsets.Add(Vector3.right);
			return;
		}

		// If ghost didn't change direction we save some processing power by skipping this
		if (CharacterMotor.CurrentDirection == newDirection) return;

		SetDirection(newDirection);
	}

	/// <summary>
	/// Filters all offsets and updates the valid tiles with just the allowed moves
	/// </summary>
	private void GetValidTiles()
	{
		_validPositions.Clear();

		foreach(var offset in _offsets)
		{
			if(CharacterMotor.IsValidPosition(_currentTileWorldPosition + offset))
			{
				_validPositions.Add(_currentTileWorldPosition + offset);
			}
		}
	}

	/// <summary>
	/// Different from players, ghosts have to perfectly align position with tiles before choosing a NextTile and thus changing direction
	/// </summary>
	private void UpdateTargetTile()
	{
		var offset = CharacterMotor.GetOffset();

		if (transform.position == _currentTileWorldPosition)
		{
			CharacterMotor.NextTile = CharacterMotor.Map.WorldToCell(transform.position + offset);
		}
	}

	public void Chase()
	{
		if (!frightened && GhostState != GhostState.Defeated)
		{
			currentGetTargetMethod = ChaseMethod;
			InverseDirection();
		}
		isChasing = true;
	}

	public void Scatter()
	{
		if (!frightened && GhostState != GhostState.Defeated)
		{
			currentGetTargetMethod = ScatterMethod;
			InverseDirection();
		}
		isChasing = false;
	}

	private void InverseDirection()
	{
		switch (CharacterMotor.CurrentDirection)
		{
			default:
			case Direction.NONE:
				break;

			case Direction.UP:
				//CharacterMotor.CurrentDirection = Direction.DOWN;
				SetDirection(Direction.DOWN);
				break;

			case Direction.DOWN:
				if (!LockUpDirection || freeMove)
				{
					//CharacterMotor.CurrentDirection = Direction.UP;
					SetDirection(Direction.UP);
				}
				break;

			case Direction.LEFT:
				//CharacterMotor.CurrentDirection = Direction.RIGHT;
				SetDirection(Direction.RIGHT);
				break;

			case Direction.RIGHT:
				//CharacterMotor.CurrentDirection = Direction.LEFT;
				SetDirection(Direction.LEFT);
				break;
		}
	}

	private void SetDirection(Direction newDirection)
	{
		CharacterMotor.CurrentDirection = newDirection;

		_offsets.Clear();
		switch (CharacterMotor.CurrentDirection)
		{
			case Direction.UP:
				_offsets.Add(Vector3.up);
				_offsets.Add(Vector3.left);
				_offsets.Add(Vector3.right);
				break;

			case Direction.DOWN:
				_offsets.Add(Vector3.left);
				_offsets.Add(Vector3.down);
				_offsets.Add(Vector3.right);
				break;

			case Direction.RIGHT:
				_offsets.Add(Vector3.up);
				_offsets.Add(Vector3.down);
				_offsets.Add(Vector3.right);
				break;

			case Direction.LEFT:
				_offsets.Add(Vector3.up);
				_offsets.Add(Vector3.left);
				_offsets.Add(Vector3.down);
				break;
		}
	}

	public void RunAway(float runAwayDuration, bool vulnerable)
	{
		if (inGhostHouse) return;

		if(vulnerable)
		{
			if (runAwayDuration > FreeEndingWarningTime)
			{
				GhostState = GhostState.Vulnerable;
			}
			else
			{
				GhostState = GhostState.VulnerabilityEnding;
			}
		}

		vulnerabilityEndTime = Time.time + runAwayDuration;
		currentGetTargetMethod = FrightenedMethod;
		InverseDirection();
		CharacterMotor.CurrentSpeedPercentage = 0.5f;
		frightened = true;
	}

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (isVulnerable && collision.CompareTag("Player"))
		{
			CharacterMotor.Map = GhostsOnlyTilemap;
			currentGetTargetMethod = DefeatedMethod;
			CharacterMotor.CurrentSpeedPercentage = 1.5f;
			GhostState = GhostState.Defeated;
			frightened = false;
			freeMove = true;
			OnDefeat?.Invoke(this);
			ghostCaughtEvent.Play(audioSource);
		}
	}

	public void Recover()
	{
		if (GhostState == GhostState.Defeated)
		{
			currentGetTargetMethod = LeaveGhostHouseMethod;

			InverseDirection();
			CharacterMotor.CurrentSpeedPercentage = 0.75f;
			GhostState = GhostState.Active;
			freeMove = false;
		}
	}

	public void ApplyCurrentGhostMethod()
	{
		inGhostHouse = false;
		if (isChasing)
		{
			currentGetTargetMethod = ChaseMethod;
		}
		else
		{
			currentGetTargetMethod = ScatterMethod;
		}
	}

	public void Deactivate()
	{
		CharacterMotor.enabled = false;
		StartCoroutine(DeathCoroutine());
		CharacterMotor.CurrentSpeedPercentage = 0.75f;
	}

	public void HideGhost()
	{
		GetComponentInChildren<SpriteRenderer>().enabled = false;
	}

	public void ShowGhost()
	{
		GetComponentInChildren<SpriteRenderer>().enabled = true;
	}

	private IEnumerator DeathCoroutine()
	{
		yield return new WaitForSeconds(1f);
		HideGhost();
	}

	public void ResetGhost()
	{
		ShowGhost();
		CharacterMotor.enabled = true;

		CharacterMotor.CurrentSpeedPercentage = 0.75f;
		GhostState = GhostState.Active;

		CharacterMotor.Map = GhostsOnlyTilemap;

		transform.position = initialPosition;
		CharacterMotor.NextTile = CharacterMotor.Map.WorldToCell(transform.position);
		UpdateTargetTile();
		
		UpdateCurrentTile(CharacterMotor.Map.WorldToCell(transform.position));

		currentGetTargetMethod = LeaveGhostHouseMethod;
		//currentGetTargetMethod = ScatterMethod;

		ChangeDirection(Direction.LEFT, true);
		ChooseDirection();

		isChasing = false;
		frightened = false;

		LockUpDirection = false;
	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		if (CharacterMotor == null) return;

		Gizmos.color = Color.red;

		Gizmos.DrawCube(targetPosition + CharacterMotor.CellOffset, Vector3.one);
	}
#endif
}
