﻿using UnityEngine;

[RequireComponent(typeof(Pacman))]
public class CharacterInput : MonoBehaviour
{
	private Pacman pacmanBehaviour;

    void Start()
    {
		pacmanBehaviour = GetComponent<Pacman>();
	}

	void Update()
	{
		// I don't want to use Axis for this, I want to work with KeyDown changing directions
		if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
		{
			pacmanBehaviour.DesiredDiection = Direction.LEFT;
		}

		if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			pacmanBehaviour.DesiredDiection = Direction.RIGHT;
		}

		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			pacmanBehaviour.DesiredDiection = Direction.UP;
		}

		if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			pacmanBehaviour.DesiredDiection = Direction.DOWN;
		}
	}
}
