﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterMotor))]
public class Pacman : MonoBehaviour
{
	private float energyzeEndTime;
	private bool energized;
	private bool alive;
	private Vector3 startPosition;

	public CharacterMotor CharacterMotor { get; private set; }

	public Action OnDeath;

	public PacmanView view;

	public AudioSource audioSource;

	public SimpleAudioEvent pacmanDefeatedEvent;

	[HideInInspector]
	public Direction DesiredDiection = Direction.NONE;

	private void Start()
	{
		CharacterMotor = GetComponent<CharacterMotor>();
		CharacterMotor.CurrentSpeedPercentage = 0.8f;
		energized = false;
		startPosition = transform.position;
		alive = true;
	}

	private void Update()
	{
		if(energized && Time.time > energyzeEndTime)
		{
			CharacterMotor.CurrentSpeedPercentage = 0.8f;
			energized = false;
		}
	}

	private void FixedUpdate()
    {
		UpdateCurrentDirection();
		UpdateTargetTile();
	}

	/// <summary>
	/// Only update the CurrentDirection if the desiredDirection is valid, this way player keeps moving until it finds a valid turn or hits the wall
	/// </summary>
	private void UpdateCurrentDirection()
	{
		var offset = CharacterMotor.GetOffset(DesiredDiection);

		if (CharacterMotor.IsValidPosition(transform.position + offset))
		{
			CharacterMotor.CurrentDirection = DesiredDiection;
		}
	}

	/// <summary>
	/// As soon as our current direction becomes valid we change our NextTile. This allows pacman to turn faster than ghosts.
	/// </summary>
	private void UpdateTargetTile()
	{
		var offset = CharacterMotor.GetOffset();
		
		if (CharacterMotor.IsValidPosition(transform.position + offset))
		{
			CharacterMotor.NextTile = CharacterMotor.Map.WorldToCell(transform.position + offset);
		}
	}

	public void Energyze(float duration)
	{
		CharacterMotor.CurrentSpeedPercentage = 0.9f;
		energyzeEndTime = Time.time + duration;
		energized = true;
	}

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (!alive) return;

		var ghost = collision.GetComponent<Ghost>();

		if(ghost != null && !ghost.isVulnerable && ghost.GhostState != GhostState.Defeated)
		{
			CharacterMotor.enabled = false;
			alive = false;
			OnDeath?.Invoke();
			StartCoroutine(DeathCoroutine());
		}
	}

	private IEnumerator DeathCoroutine()
	{
		yield return new WaitForSeconds(1.2f);
		pacmanDefeatedEvent.Play(audioSource);
		view.PlayDeathAnimation();
	}

	public void ResetPacman()
	{
		CharacterMotor.enabled = true;
		alive = true;
		transform.position = startPosition;
		CharacterMotor.NextTile = CharacterMotor.Map.WorldToCell(transform.position);
		DesiredDiection = Direction.NONE;
		CharacterMotor.CurrentDirection = Direction.NONE;
		view.EndDeathAnimation();
	}

	public void HidePacman()
	{
		view.Hide();
	}

	public void ShowPacman()
	{
		view.Show();
	}
}
