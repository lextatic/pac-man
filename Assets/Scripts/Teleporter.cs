﻿using UnityEngine;

public class Teleporter : MonoBehaviour
{
	public Transform exitPosition;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		collision.transform.position = exitPosition.position;

		// Update NextTile after teleport
		var motor = collision.GetComponent<CharacterMotor>();
		motor.NextTile = motor.Map.WorldToCell(collision.transform.position);
	}
}
